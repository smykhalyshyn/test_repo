import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Cin {

	 public static String String() {                                                

		  BufferedReader br = new BufferedReader(new InputStreamReader(System.in));  //new buffered reader
		  String input="";              //initializing variable input

		  try {
		  input = (br.readLine());        //reading the entered line
		  } catch (IOException e) {       //in case some exception xD
		  e.printStackTrace();
		  }

		  return input;               
		  }
	
}